Released Phase 1 novelties:

1. Level 11 Type 50: Styroform Black (wood health x 4, mass reduce 90%)
    - corresponding pre-novelty games: level 0 type 1150

2. Level 11 Type 130: Orange bird wood damage 0, all other parameters same as yellow birds
    - corresponding pre-novelty games: level 0 type 11130

3. Level 12 Type 30: Yellow bird launch gravity increase to 0.7, i.e., the max reachable distance is shorter
    - corresponding pre-novelty games: level 0 type 1230

4. Level 12 Type 110: Blue Bird Split to 5
    - corresponding pre-novelty games: level 0 type 12110

5. Level 13 Type 20: Remove blue channel (the last two bits) of the colours of objects.
    - corresponding pre-novelty games: level 0 type 1320

6. Level 13 Type 90: Shift the coordinate origin to the center point of the slingshot
    -corresponding pre-novelty games: level 0 type 1390

Released Phase 2 novelties:

1. Level 22 Type 40-44: Bird Teleporters that send a contacted object from one gate to the other
    - corresponding pre-novelty games: level 0 type 2240-2244

2. Level 22 Type 50-54: A space invader that moving horizontally and hits the directly reachable objects underneath with a laser beam
    - corresponding pre-novelty games: level 0 type 2250-2254

3. Level 23 Type 40: The witch is a bird chaser now that actively speeds up and attempts to hit any birds that is within a certain distance  
    - corresponding pre-novelty games: level 0 type 2342

4. Level 23 Type 70: The witch is an immortalizing agent that makes all the pigs immortal if it is killed
    - corresponding pre-novelty games: level 0 type 2373

5. Level 24 Type 60-64: The TNT is now a bird nest that will provide two more red birds if it is destroyed
    - corresponding pre-novelty games: level 0 type 2460-2464

6. Level 24 Type 70-74: The red bird can go through platforms
    - corresponding pre-novelty games: level 0 type 2470-2474

7. Level 25 Type 60-63: The platforms will move up and down
    - corresponding pre-novelty games: level 0 type 2560-2563

8. Level 25 Type 70-74: The TNT power cannot go through the platforms anymore
    - corresponding pre-novelty games: level 0 type 2570-2574

Sample Phase 3 novelties

1. Level 36 Type 10-14: No gravity for birds
    - corresponding pre-novelty games: level 0 type 3610-3614

2. Level 36 Type 20-24: A radioactive area that will kill any bird that touches it
    - corresponding pre-novelty games: level 0 type 3620-3624

3. Level 37 Type 10: The magician acts as a transmuter who turns any touched object to a pig
    - corresponding pre-novelty games: level 0 type 3711

4. Level 37 Type 20: The butterfly adds health points to the pigs nearby
    - corresponding pre-novelty games: level 0 type 3723

5. Level 38 Type 10-14: A time triggered storm that applies force to the left of the flying birds
    - corresponding pre-novelty games: level 0 type 3810-3814

6. Level 38 Type 20-24: A long wood object that will destroy itself when a bird collides with any object  
    - corresponding pre-novelty games: level 0 type 3820-3824

Phase 3 Evaluation Novelties

1. Level 36 Type 30-34
    - A mask is applied to the colour maps of all objects

2. Level 36 Type 70-74
    - The birds become heavier and the trajectory becomes shorter

3. Level 36 Type 80-84
    - There is a bushfire that will burn the higher pig after some time

4. Level 37 Type 40
    - The butterfly will move the pig to another place when it flies over the pig

5. Level 37 Type 70
    - The witch will kill the bird on sling if it moves very close to the sling

6. Level 37 Type 80
    - The worm will spawn a new pig when it reaches to the existing pig

7. Level 38 Type 60-64
    - A mask is applied to the colour maps of all objects periodically

8. Level 38 Type 70-74
    - Flood comes after hitting the novel object and all free objects (except birds and pigs) will float

9. Level 38 Type 80-84
    - The rest birds will change to yellow birds after hitting the novel object
